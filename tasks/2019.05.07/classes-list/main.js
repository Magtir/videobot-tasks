$(function () {
  resize();

  $( window ).resize(function() {
    resize();
  });

  $(document)
      .on('change', '#js-list-classes__search-date', function () {
        let valArr = $(this).val().split('-');
        let date = valArr[2] + '.' + valArr[1] + '.' + valArr[0];
        console.log(date);
      })
});

function resize() {
  let $tbody = $('#js-list-classes__tbody');
  let windowHeight = $(window).height();
  let positionTop = $tbody.offset().top;
  $tbody.css('max-height', windowHeight - positionTop - 70);
}