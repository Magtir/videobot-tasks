$(function () {
  $(document)
      .on('click', '#js-table-passing-time-hide-show', function () {
        $('#holder-table-passing-time').toggleClass('drop');
      })
      .on('click', '.button-table-sort', function () {
        let $this = $(this);
        let $buttonTableSort = $('.button-table-sort');

        let thisText = $this.attr('data-text');
        let thisOrder = $this.attr('data-order');
        let thisField = JSON.parse($this.attr('data-field'));

        if (!thisOrder) {
          $buttonTableSort.each(function (i, item) {
            let $item = $(item);
            let itemText = $item.attr('data-text');

            $item.attr('data-order', '');
            $item.html(itemText);
          });

          $this.attr('data-order', 'ASC');
          $this.html(thisText + ' ↓');

          sortObj.field = thisField;
          sortObj.order = 'ASC';
        } else {
          switch (thisOrder) {
            case 'ASC':
              $this.attr('data-order', 'DESC');
              $this.html(thisText + ' ↑');

              sortObj.order = 'DESC';
              break;

            case 'DESC':
              $this.attr('data-order', '');
              $this.html(thisText);

              sortObj.field = [];
              sortObj.order = '';
          }
        }

        fillTimeReport();
      });

  // где нибудь захардкодь данные с сервера, что бы js мог с ними работать
  // внешне let report;
  // когда получишь данные
  // report = data.report;
  // после уже запускай функцию fillTimeReport(), она будет работать с этой переменной, как и все сортировки

  fillTimeReport();
});
