function fillTimeReport() {
  let report = sort();

  let rows = '';
  report.forEach(
      row => {
        let fromDate = new Date(row.fromAbs);
        let fromTimeStr = fixIfLessThan10(fromDate.getHours())
            + ":"
            + fixIfLessThan10(fromDate.getMinutes())
            + ":"
            + fixIfLessThan10(fromDate.getSeconds());
        let toTimeStr = "--";
        if (row.toAbs) {
          let toDate = new Date(row.toAbs);
          toTimeStr = fixIfLessThan10(toDate.getHours())
              + ":"
              + fixIfLessThan10(toDate.getMinutes())
              + ":"
              + fixIfLessThan10(toDate.getSeconds());
        }
        let totalTimeStr = "--";
        if (row.totalMillis) {
          totalTimeStr = Math.floor(row.totalMillis / 60000)
              + 'мин. '
              + Math.floor(row.totalMillis % 60000 / 1000)
              + 'сек.';
        }
        let normalTimeStr = "--";
        if (row.normalTime) {
          normalTimeStr = session.exercise.normalTime + ' мин.';
        }
        rows += `<tr>
                        <td>${ row.student.name }</td>
                        <td>${ fromTimeStr }</td>
                        <td>${ toTimeStr }</td>
                        <td>${ totalTimeStr }</td>
                        <td>${ normalTimeStr }</td>
                    </tr>`
      }
  );
  $("#time-report-tbody")
      .empty()
      .append(rows);
}

function fixIfLessThan10(value) {
  return value < 10 ? '0' + value : value;
}
