let sortObj = {
  field: [],
  order: '',
};

function sort() {
  let copy = JSON.parse(JSON.stringify(report));

  if (sortObj.field.length) {
    copy.sort(function (a, b) {
      let fieldA = getValue(a);
      let fieldB = getValue(b);

      if (typeof fieldA === "string") {
        fieldA = fieldA.toLowerCase();
        fieldB = fieldB.toLowerCase();
      }

      if (fieldA < fieldB) {
        if (sortObj.order === 'ASC') {
          return -1;
        } else {
          return 1;
        }
      }

      if (fieldA > fieldB) {
        if (sortObj.order === 'ASC') {
          return 1;
        } else {
          return -1;
        }
      }

      return 0;
    });
  }

  return copy;
}

function getValue(elemet) {
  let val = elemet;
  sortObj.field.forEach(function (f) {
    val = val[f];
  });

  return val;
}
