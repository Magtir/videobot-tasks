// основной компонент, который связывает таблицу и форму

class RfidCards {
  constructor() {
    // init полей
    // инициализация компонента таблицы
    this.callbackCopyCardAndWrite = card => this.blockAddComponent.fillFormAndApiSend(card);
    this.tableComponent = new RfidCardsTable(this.setLoading.bind(this), this.callbackCopyCardAndWrite.bind(this));

    this.callbackAddCard = card => this.tableComponent.addCards([card]);
    this.blockAddComponent = new RfidCardsBlockAdd(this.setLoading.bind(this), this.callbackAddCard.bind(this));

    this.hostElem;
    this.btnCloseElem;
    this.loaderElem;

    this.isLoading = true;
    // я не знаю, как на jq можно объединять запросы, по этому эта переменная под два основных запроса. Если оба будут true, то можно работать дальше
    this.dataInited = [false, false];
    // init полей end

    // инициализация компонента
    this.hostElem = document.querySelector('#rfid-cards-dz');
    if (this.hostElem) {
      this.btnCloseElem = this.hostElem.querySelector('#rfid-cards__head__btn-close');
      this.loaderElem = this.hostElem.querySelector('.rfid-cards__loader');
    }

    // если с инициализацией всё ок, запускаем подписки и апишки
    if (this.hostElem && this.btnCloseElem && this.loaderElem) {
      // прослушка события клика по крестику - вызовет метод close();
      this.btnCloseElem.onclick = () => this.close();

      // вешаем загрузочную ширму
      this.setLoading(true);
      // вызываем api для получения карт
      this.apiGetCards();
      // вызываем api для получения устройств
      this.apiGetDevices();

      // инициализируешь вебсокет
      this.initWS();
    } else {
      console.error('Не все элементы найдены');
    }
  }

  defineDataInited() {
    return this.dataInited.every(i => i);
  }

  // клик по крестику
  close() {
    console.log('Нажат крестик');
  }

  // установка загрузочной ширмы
  setLoading(flag) {
    if (this.loaderElem) {
      if (flag) {
        this.loaderElem.classList.add('rfid-cards__loader--show');
      } else {
        this.loaderElem.classList.remove('rfid-cards__loader--show');
      }
      this.isLoading = flag;
    }
  }

  // api для получения существующих карт. Она вызывается сразу во время инициализации основного компонента (в конструкторе)
  apiGetCards() {
    // тут вызываешь api
    setTimeout(() => {
      // пришла дата с API (массив с объектами карт)
      const data = [
        { token: 145212523651, createTime: 1561332489194 },
        { token: 145288023123, createTime: 1571342689194 },
        { token: 323209526658, createTime: 1581352789194 },
      ]

      // засовываешь в метод таблицы, что бы их добавить (должен быть массив с этими моделями)
      this.tableComponent.addCards(data);

      // этот блок обязателен по финишу begin
      this.dataInited[0] = true;
      if (this.defineDataInited()) {
        this.setLoading(false);
      }
      // этот блок обязателен по финишу end

    }, 500);
  }

  // api для получения устройств. Она вызывается сразу во время инициализации основного компонента (в конструкторе)
  apiGetDevices() {
    // тут вызываешь api
    setTimeout(() => {
      // пришла дата с API (массив с объектами девайсов)
      let data = [
        // { entityId: 1, name: 'Устройство 1', online: true },
        { entityId: 2, name: 'Устройство 2', online: true },
        { entityId: 3, name: 'Устройство 3', online: false },
        { entityId: 4, name: 'Устройство 4', online: true },
      ]

      //  сортировка по online == true
      data = data.filter(d => d.online);
      // засовываешь в метод компоненты формы, что бы их добавить в select (должен быть массив с этими моделями)
      this.blockAddComponent.setDevices(data);

      // этот блок обязателен по финишу begin
      this.dataInited[1] = true;
      if (this.defineDataInited()) {
        this.setLoading(false);
      }
      // этот блок обязателен по финишу end

    }, 1000);
  }

  initWS() {
    // тут напишешь инициализацию вебсокета(ов)
    //    если пришла карта (это к примеру карта)
    // let data = {};
    //    она скорее всего придет как объект. Оберни её в массив
    // data = [data];
    //    отдай таблице, что бы она её отобразила
    // this.tableComponent.addCards(data);

    // если пришло уведомление
    this.blockAddComponent.showNotification('Привет');
  }
}

$(function () {
  new RfidCards();
});
