class RfidCardsTable {
  constructor(funcSetLoading, callbackCopyCardAndWrite) {
    // init полей
    this.hostElem;
    this.tableBodyElem;

    this.cards = [];

    this.setLoading = flag => {
    };
    this.callbackCopyCardAndWrite = card => {
    };
    // init полей end
    this.hostElem = document.querySelector('#rfid-cards__table-dz');
    if (this.hostElem) {
      this.tableBodyElem = this.hostElem.querySelector('.rfid-cards__table__body');
    }

    if (this.hostElem && this.tableBodyElem) {
      this.setLoading = funcSetLoading;
      this.callbackCopyCardAndWrite = callbackCopyCardAndWrite;
    } else {
      console.error('Не все элементы таблицы найдены');
    }
  }

  // если нужно вставлять c верху вниз, то передать в inTop false
  // если второй аргумент не передается, то по умолчанию будет добавляться с снизу вверх
  addCards(cards, inTop = true) {
    cards.forEach(card => {
      const date = new Date(card.createTime);
      const year = date.getFullYear();
      let month = date.getMonth() + 1;
      if (month < 9) {
        month = `0${ month }`
      }
      let day = date.getDay();
      if (day < 9) {
        day = `0${ day }`
      }

      card.createTime = `${ day }.${ month }.${ year }`;
      this.cards.push(card);
      this.drawRow(card, inTop);
    });
  }

  drawRow(card, inTop) {
    const newRowElem = document.createElement('div');
    newRowElem.classList.add('rfid-cards__table__row');
    newRowElem.innerHTML =
      `<div class="rfid-cards__table__cell rfid-cards__table__cell--token">${ card.token } <button class="rfid-cards__table__btn-auto"></button></div>` +
      `<div class="rfid-cards__table__cell rfid-cards__table__cell--date">${ card.createTime }</div>` +
      `<div class="rfid-cards__table__cell rfid-cards__table__cell--del"><button class="rfid-cards__table__btn-del"></button></div>`;

    const btnCopy = newRowElem.querySelector('.rfid-cards__table__btn-auto');
    btnCopy.onclick = () => {
      this.callbackCopyCardAndWrite(card);
    };

    const btnDel = newRowElem.querySelector('.rfid-cards__table__btn-del');
    btnDel.onclick = () => {
      this.apiDeleteCard(card, newRowElem);
    };

    if (inTop) {
      this.tableBodyElem.insertBefore(newRowElem, this.tableBodyElem.firstChild);
    } else {
      this.tableBodyElem.appendChild(newRowElem);
    }
  }

  // удаление карты
  apiDeleteCard(card, rowElem) {
    // вешаем загрузку
    this.setLoading(true);

    // вызываем api
    setTimeout(() => {
      // по успешности удаляем строку из HTML
      rowElem.remove();
      // по финишу снимаем лоадинг
      this.setLoading(false);

      console.log(`Удаление карты с token: ${ card.token }`);
    }, 1000);
  }
}
