class RfidCardsBlockAdd {
  constructor(funcSetLoading, callbackAddCard) {
    // init полей
    this.classMod = 'rfid-cards__block-add__not-advanced-mode';
    this.classInvalid = 'rfid-cards__block-add__form__field__input--invalid';

    this.hostElem;
    this.inputAdvancedMode;

    this.form = {
      deviceId: {
        wrapperElem: null,
        input: null,
        dataFor: 'value'
      },
      customValue: {
        wrapperElem: null,
        input: null,
        dataFor: 'value'
      },
      bindToUser: {
        wrapperElem: null,
        input: null,
        dataFor: 'checked'
      },
    };

    this.inputNotification;

    this.btnsWrapperElem;
    this.btnFirstElem;
    this.btnSecondElem;

    this.isAdvancedMode = false;

    this.setLoading = flag => {
    };
    this.callbackAddCard = card => {
    };
    // init полей end

    this.hostElem = document.querySelector('#rfid-cards__block-add-dz');
    if (this.hostElem) {
      this.inputAdvancedMode = this.hostElem.querySelector('input[name=advanced-mode]');

      this.form.deviceId.wrapperElem = this.hostElem.querySelector('#field-device-id');
      if (this.form.deviceId.wrapperElem) {
        this.form.deviceId.input = this.form.deviceId.wrapperElem.querySelector('select[name=device-id]');
        if (this.form.deviceId.input) {
          this.form.deviceId.input.oninput = () => this.form.deviceId.input.classList.remove(this.classInvalid);
        }
      }

      this.form.customValue.wrapperElem = this.hostElem.querySelector('#field-custom-value');
      if (this.form.customValue.wrapperElem) {
        this.form.customValue.input = this.form.customValue.wrapperElem.querySelector('input[name=custom-value]');
        if (this.form.customValue.input) {
          this.form.customValue.input.oninput = () => this.form.customValue.input.classList.remove(this.classInvalid);
          // todo это раскоменти для маски. Не разбирался как она работает
          // this.form.customValue.input.masked = IMask(
          //   this.form.customValue.input,
          //   {
          //     lazy: false,
          //     mask: '##:##:##:##:##',
          //     definitions: {
          //       '#': /[0-9a-fA-F]/
          //     }
          //   });
        }
      }

      this.form.bindToUser.wrapperElem = this.hostElem.querySelector('#field-bind-to-user');
      if (this.form.bindToUser.wrapperElem) {
        this.form.bindToUser.input = this.form.bindToUser.wrapperElem.querySelector('input[name=bind-to-user]');
      }

      this.btnsWrapperElem = this.hostElem.querySelector('.rfid-cards__block-add__form__btns-wrapper');
      if (this.btnsWrapperElem) {
        this.btnFirstElem = this.btnsWrapperElem.querySelector('#rfid-cards__block-add__form__btn--first');
        this.btnSecondElem = this.btnsWrapperElem.querySelector('#rfid-cards__block-add__form__btn--second');
      }

      this.inputNotification = this.hostElem.querySelector('textarea[name=notifications]');
    }

    let valid = true;

    Object.keys(this.form).forEach(keyField => {
      if (!this.form[keyField].input) {
        valid = false;
      }
    });

    if (valid && (!this.hostElem || !this.inputAdvancedMode || !this.btnFirstElem || !this.btnSecondElem || !this.inputNotification)) {
      valid = false;
    }

    if (valid) {
      this.inputAdvancedMode.oninput = () => {
        this.setAdvanceMode(this.inputAdvancedMode.checked);
      }

      this.inputNotification.oninput = () => {
        this.defineValueTextarea();
      }

      this.btnFirstElem.onclick = () => {
        this.clickAddCard();
      }

      this.btnSecondElem.onclick = () => {
        this.clickRead();
      }

      this.setLoading = funcSetLoading;
      this.callbackAddCard = callbackAddCard;
    } else {
      console.error('Не все элементы формы найдены');
    }
  }

  defineValueTextarea() {
    if (this.inputNotification.value !== '') {
      this.inputNotification.classList.add('rfid-cards__block-add__form__textarea--focus');
    } else {
      this.inputNotification.classList.remove('rfid-cards__block-add__form__textarea--focus');
    }
  }

  setAdvanceMode(flag) {
    if (flag) {
      this.form.customValue.wrapperElem.classList.remove(this.classMod);
      this.form.bindToUser.wrapperElem.classList.remove(this.classMod);
      this.btnsWrapperElem.classList.remove(this.classMod);
      this.btnFirstElem.classList.remove(this.classMod);
      this.btnFirstElem.innerHTML = 'Записать';
      this.btnSecondElem.classList.remove(this.classMod);
    } else {
      this.form.customValue.wrapperElem.classList.add(this.classMod);
      this.form.bindToUser.wrapperElem.classList.add(this.classMod);
      this.btnsWrapperElem.classList.add(this.classMod);
      this.btnFirstElem.classList.add(this.classMod);
      this.btnFirstElem.innerHTML = 'Добавить карту';
      this.btnSecondElem.classList.add(this.classMod);
    }

    this.isAdvancedMode = flag;
  }

  // добавить девайсы (массив)
  // если не будет ни одного девайса, то оставит только один вариант "Нет доступных"
  // если будет только один, то сразу выберется
  // если будет несколько, то нужно будет выбрать
  setDevices(devices) {
    if (devices.length) {
      this.form.deviceId.input[0].remove();
      if (devices.length > 1) {
        const option = document.createElement('option');
        option.text = 'Выберите устройство';
        option.value = '';
        this.form.deviceId.input.add(option);
      }

      devices.forEach(device => {
        const option = document.createElement('option');
        option.text = device.name;
        option.value = device.entityId;
        this.form.deviceId.input.add(option);
      })
    }
  }

  validForm(command) {
    let valid = true;

    if (!this.form.deviceId.input.value) {
      this.form.deviceId.input.classList.add(this.classInvalid);
      valid = false;
    } else {
      this.form.deviceId.input.classList.remove(this.classInvalid);
    }

    // если это не команда "Считать" и это расширенный решим
    if (command !== 'READ' && this.isAdvancedMode) {
      // то валидируем и Tag
      if (valid) {
        // todo раскоменти, когда ты добавишь маску
        // вроде правильно написал, но мог ошибиться. Перепроверяй и тести
        // if (this.form.customValue.input.masked.unmaskedValue && !this.form.customValue.input.masked.unmaskedValue.match(/^[0-9a-fA-F]{10}$/i)) {
        //   this.form.customValue.input.classList.add(this.classInvalid);
        //   valid = false;
        // } else {
        //   this.form.customValue.input.classList.remove(this.classInvalid);
        // }
      }
    }

    return valid;
  }

  // отдаст заполненную модель из формы
  getModelByForm(command) {
    const model = { command };

    // если команда не "Считать", то берем все поля
    if (command !== 'READ') {
      Object.keys(this.form).forEach(keyField => {
        const field = this.form[keyField];
        model[keyField] = field.input[field.dataFor];
      });
    } else {
      model.deviceId = this.form.deviceId.input.value;
    }

    model.deviceId = +model.deviceId;

    return model;
  }

  // очистить форму
  resetForm() {
    Object.keys(this.form).forEach(keyField => this.form[keyField].input[this.form[keyField].dataFor] = '');
  }

  clickAddCard() {
    // при нажатии на "добавить карту", вызываем метод валидации формы и отправки api с командой WRITE
    this.validFormAndApiSend('WRITE');
  }

  clickRead() {
    // при нажатии на "считать", вызываем метод валидации формы и отправки api с командой READ
    this.validFormAndApiSend('READ');
  }

  validFormAndApiSend(command) {
    // первым делом валидируем форму
    // если что-то будет не валидно, оно подстветит
    if (this.validForm(command)) {
      // если форма валидна
      // вешаем лоадер
      this.setLoading(true);

      // получаем модель из формы
      const model = this.getModelByForm(command);

      // если упрощенный режим
      if (!this.isAdvancedMode) {
        model.bindToUser = true;
      }

      // сказали, что если bindToUser == true, то надо что-то дозаполнять
      if (model.bindToUser) {
        // тут дозаполняем что надо, если bindToUser == true
        model.additional = 'qwe';
      }

      // отправляем в api
      this.apiSend(model);
    }
  }

  // метод для заполнения формы по модели карты и её отправки
  fillFormAndApiSend(card) {
    // валидируем только deviceId
    if (this.validForm('READ')) {
      // если девайс выбран
      // вешаем лоадинг
      this.setLoading(true);

      // заполянем форму
      this.form.customValue.input.value = card.token;
      this.form.bindToUser.input.checked = false;

      // получаем модель из формы
      const model = this.getModelByForm('WRITE');

      // отправляем в api
      this.apiSend(model);
    }
  }

  apiSend(model) {
    console.log('Отправка модели:', model);

    // тут вызываешь api и отдаешь модель
    setTimeout(() => {
      // если всё ок, или сам смотри когда лучше и надо ли вообще - очищаем форму
      this.resetForm();

      // по финалу api убираешь лоадер
      this.setLoading(false);
    }, 1000);
  }

  showNotification(string) {
    this.inputNotification.value = string;
    this.defineValueTextarea();
  }
}
