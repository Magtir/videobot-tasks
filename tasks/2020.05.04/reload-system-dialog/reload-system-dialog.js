class ReloadSystemDialog {
  constructor() {
    // init полей
    this.hostElem = document.querySelector('#reload-system-dialog-dz');
    this.btnClose = this.hostElem.querySelector('#reload-system-dialog__head__btn-close');
    this.btnCancel = this.hostElem.querySelector('#reload-system-dialog__footer__btn--cancel');
    this.btnOk = this.hostElem.querySelector('#reload-system-dialog__footer__btn--ok');
    // init полей end

    if (this.hostElem && this.btnClose && this.btnCancel && this.btnOk) {
      this.btnClose.onclick = () => {
        console.log('close');
      };

      this.btnCancel.onclick = () => {
        console.log('cancel');
      };

      this.btnOk.onclick = () => {
        console.log('ok');
      }
    } else {
      console.error('Не все элементы найдены');
    }
  }
}

$(function () {
  new ReloadSystemDialog();
});
