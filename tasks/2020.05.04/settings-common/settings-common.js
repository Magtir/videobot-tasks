const TEST_MODEL = {
  nameServer: 'QWERTY',
  frequency: 10,
  limit: 2.2,
  warning: 1.6,
  kurento: 'https://google.com',
  ice: ['http://google.com'],
  fileSyncPriority: 'slave',
  fileSynchronizeMedia: true,
  fileSyncDelay: 12
};
const TEST_ERROR = {
  errors: [
    {
      error: {
        message: "Parameter N is invalid",
        type: "INVALID_FIELD",
        field: "ice"
      }
    }
  ]
}

class SettingsCommon {
  constructor() {
    // init полей
    this.hostElem = document.querySelector('#settings-common-dz');
    this.formGroup = {
      nameServer: { input: null, errorElem: null },
      frequency: { input: null, errorElem: null },
      limit: { input: null, errorElem: null },
      warning: { input: null, errorElem: null },
      kurento: { input: null, errorElem: null },
      ice: { input: null, errorElem: null },
      fileSyncPriority: { input: null, errorElem: null },
      fileSynchronizeMedia: { input: null, errorElem: null },
      fileSyncDelay: { input: null, errorElem: null }
    };

    this.settings = {};
    this.currentModIsEdit = false;
    // init полей end

    if (this.hostElem) {
      this.formElem = this.hostElem.querySelector('#settings-common__form');
      this.wrapperInputsIceElems = this.hostElem.querySelector('#settings-common-form__text-input__group');
      this.btnChangeModElem = this.hostElem.querySelector('#settings-common-btn-change-mod');
      this.btnAddIceElem = this.hostElem.querySelector('#settings-common-form__btn-add-ice');

      Object.keys(this.formGroup).forEach(key => {
        this.formGroup[key].input = this.formElem.elements[key];
        this.formGroup[key].errorElem = this.hostElem.querySelector(`#settings-common__form__field-error--${ key }`);
      });

      if (this.wrapperInputsIceElems && this.btnChangeModElem && this.btnAddIceElem && this.defineFormIsReady()) {
        this.subscribeResetInvalidInput();
        this.apiGetSettings();
        this.btnChangeModElem.onclick = () => this.currentModIsEdit ? this.apiSave() : this.changeMod();
        this.btnAddIceElem.onclick = () => this.addIce();
      } else {
        console.error(`Не удалось проинициализировать компонент`);
      }
    }
  }

  defineFormIsReady() {
    let isReady = true;
    Object.keys(this.formGroup).forEach(key => {
      if (!this.formGroup[key].input || !this.formGroup[key].errorElem) {
        isReady = false;
        console.error(`Ошибка при инициализации поля формы ${ key }`);
      }
    });
    return isReady;
  }

  // api запрос на настройки
  apiGetSettings() {
    this.setLoading(true);
    setTimeout(() => {
      // пришла с api модель (TEST_MODEL)
      console.log(`Получили модель:`, TEST_MODEL);
      this.settings = TEST_MODEL;
      this.fillFormByModel();
      this.setLoading(false);
    }, 1000)
  }

  // заполнение формы из модели
  fillFormByModel() {
    if (this.settings.ice && this.settings.ice.length) {
      this.settings.ice.forEach((val, i) => {
        if (i) {
          this.addIce();
        }
      });
      this.formGroup.ice.input = this.formElem.elements['ice'];
    }

    Object.keys(this.formGroup).forEach(key => {
      if (this.settings[key] && this.settings[key] instanceof Array && this.settings[key].length > 1) {
        this.settings[key].forEach((val, i) => {
          if (this.formGroup[key].input[i].type === 'checkbox') {
            this.formGroup[key].input[i].checked = val
          } else {
            this.formGroup[key].input[i].value = val
          }
        });
      } else {
        if (this.formGroup[key].input.type === 'checkbox') {
          this.formGroup[key].input.checked = this.settings[key] || false
        } else {
          this.formGroup[key].input.value = this.settings[key] || ''
        }
      }
    });
  }

  // прослушки событий изменения полей, для снятия невалидности
  subscribeResetInvalidInput() {
    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.length) {
        group = [group];
      }

      // если есть forEach, то это обычная коллекция
      if (group.forEach) {
        group.forEach(input => input.oninput = () => this.setErrorToField(key, false));

        // иначе это select
      } else {
        group.oninput = () => this.setErrorToField(key, false);
      }
    });
  }

  // смена режима редактирования
  changeMod() {
    this.currentModIsEdit = !this.currentModIsEdit;

    const removeBtnsElems = document.querySelectorAll('.settings-common-form__text-input__btn-remove');

    if (this.currentModIsEdit) {
      this.btnChangeModElem.innerHTML = 'Сохранить';
      this.hostElem.classList.remove('settings-common--readonly');
      this.btnAddIceElem.classList.remove('settings-common-form__btn-add-ice--readonly');
      if (removeBtnsElems) {
        removeBtnsElems.forEach(btn => btn.classList.remove('settings-common-form__text-input__btn-remove--readonly'));
      }
    } else {
      this.btnChangeModElem.innerHTML = 'Изменить';
      this.hostElem.classList.add('settings-common--readonly');
      this.btnAddIceElem.classList.add('settings-common-form__btn-add-ice--readonly');
      if (removeBtnsElems) {
        removeBtnsElems.forEach(btn => btn.classList.add('settings-common-form__text-input__btn-remove--readonly'));
      }
    }

    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.length) {
        group = [group];
      }

      // если есть forEach, то это обычная коллекия
      if (group.forEach) {
        group.forEach(input => !this.currentModIsEdit ? input.setAttribute('readonly', true) : input.removeAttribute('readonly'));

        // иначе это select
      } else {
        !this.currentModIsEdit ? group.setAttribute('readonly', true) : group.removeAttribute('readonly')
      }
    });
  }

  // сохранение модели
  apiSave() {
    if (this.formValid()) {
      this.setLoading(true);
      const model = this.getModalByForm();
      console.log(`Сохранение модели:`, model);

      setTimeout(() => {
        // тут пришла ошибка от бека с невалидным полем
        const errors = TEST_ERROR.errors;
        this.parseError(errors);
        // если ошибки не было, то переключаем мод
        if (!errors) {
          this.changeMod();
        } else {
          // в любом случае по финалу вырубаем лоадинг
          this.setLoading(false);
        }
      }, 1000);
    }
  }

  // валидация формы
  formValid() {
    let isValid = true;
    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.length) {
        group = [group];
      }

      // если есть forEach, то это обычная коллекция
      if (group.forEach) {
        group.forEach(input => {
          if (input.name !== 'fileSynchronizeMedia') {
            if (!input.value) {
              this.setErrorToField(key, 'Обязательно для заполнения')
              isValid = false;
            }
          }
        });

        // иначе это select
      } else {
        if (!group.value) {
          this.setErrorToField(key, 'Обязательно для заполнения')
          isValid = false;
        }
      }
    });

    return isValid;
  }

  parseError(errors) {
    // бежим по ошибкам, где есть свойство field
    errors.filter(err => err.error && err.error.field).forEach(err => {
      let textError;
      // в зависимости от типа ошибки готовим её текст
      switch (err.error.type) {
        case 'INVALID_FIELD':
          textError = 'Некорректное значение';
      }

      // отдаем текст ошибки, что бы отобразить
      this.setErrorToField(err.error.field, textError);
    })
  }

  setErrorToField(key, textError) {
    let group = this.formGroup[key].input;
    if (!group.length) {
      group = [group];
    }

    if (textError) {
      this.formGroup[key].errorElem.innerHTML = textError;
      this.formGroup[key].errorElem.classList.add('settings-common__form__field-error--active');
      group.forEach ? group.forEach(input => input.classList.add('invalid')) : group.classList.remove('invalid');
    } else {
      this.formGroup[key].errorElem.classList.remove('settings-common__form__field-error--active');
      group.forEach ? group.forEach(input => input.classList.remove('invalid')) : group.classList.add('invalid');
    }
  }

  // получение модели из формы
  getModalByForm() {
    const model = {};
    Object.keys(this.formGroup).forEach(key => {
      const group = this.formGroup[key].input;
      if (group.length && group[0].getAttribute('type') === 'text') {
        model[key] = [];
        group.forEach(input => model[key].push(input.value));
      } else {
        if (group.type !== 'checkbox') {
          model[key] = group.value;
        } else {
          model[key] = group.checked;
        }
      }
    });
    if (!(model.ice instanceof Array)) {
      model.ice = [model.ice];
    }
    model.frequency *= 1000;
    model.fileSyncDelay *= 1000;

    model.limit *= 1024;
    model.warning *= 1024;
    return model;
  }

  addIce() {
    const field = document.createElement('label');
    let classesForRemoveBtn = 'settings-common-form__text-input__btn-remove';
    let readonly = '';
    if (!this.currentModIsEdit) {
      classesForRemoveBtn += ' settings-common-form__text-input__btn-remove--readonly';
      readonly = 'readonly'
    }
    field.setAttribute('class', 'settings-common-form__text-input__label');
    field.innerHTML = `
               <input type="text" class="settings-common-form__text-input__input" placeholder="Адрес сервера" name="ice" ${ readonly }>
                <button type="button" class="${ classesForRemoveBtn }">
                  <i class="glyphicon glyphicon-trash"></i>
                </button>`;
    const btnRemove = field.querySelector('.settings-common-form__text-input__btn-remove');
    btnRemove.onclick = () => {
      if (btnRemove.parentNode) {
        btnRemove.parentNode.remove();
      }
    };
    this.wrapperInputsIceElems.appendChild(field);
    this.subscribeResetInvalidInput();

    // пересчитываем поля формы
    this.formGroup.ice.input = this.formElem.elements['ice'];
  }

  setLoading(flag) {
    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.length) {
        group = [group];
      }

      // если есть forEach значит обычная коллекция
      if (group.forEach) {
        group.forEach(input => flag ? input.setAttribute('disabled', 'disabled') : input.removeAttribute('disabled'));

        // если нет, то это select
      } else {
        flag ? group.setAttribute('disabled', 'disabled') : group.removeAttribute('disabled')
      }
    });

    if (this.btnChangeModElem) {
      flag ? this.btnChangeModElem.setAttribute('disabled', 'disabled') : this.btnChangeModElem.removeAttribute('disabled');
    }

    if (this.btnAddIceElem) {
      flag ? this.btnAddIceElem.setAttribute('disabled', 'disabled') : this.btnAddIceElem.removeAttribute('disabled');
    }
  }
}

$(function () {
  new SettingsCommon();
});
