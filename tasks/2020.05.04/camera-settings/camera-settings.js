const TEST_MODEL = { id: 1, name: 'Камера 2', ip: '192.12.3', port: 4200, dynamics: 'feedback', microphone: 'disable' };
const TEST_ERROR = {
  errors: [
    {
      error: {
        message: "Parameter N is invalid",
        type: "INVALID_FIELD",
        field: "password"
      }
    }
  ]
}

class CameraSettings {
  constructor(cameraId) {
    // init полей
    this.hostElem = document.querySelector('#camera-settings-dz');
    this.formGroup = {
      name: { input: null, errorElem: null },
      ip: { input: null, errorElem: null },
      port: { input: null, errorElem: null },
      password: { input: null, errorElem: null },
      dynamics: { input: null, errorElem: null },
      microphone: { input: null, errorElem: null },
    };

    this.camera = {};

    let formIsReady;
    // init полей end

    if (this.hostElem) {
      this.formElem = this.hostElem.querySelector('#camera-settings-form');
      if (this.formElem) {
        Object.keys(this.formGroup).forEach(key => {
          this.formGroup[key].input = this.formElem.elements[key];
          this.formGroup[key].errorElem = this.hostElem.querySelector(`#camera-settings-form__field-error--${ key }`);
        });

        formIsReady = this.defineFormIsReady();

        if (formIsReady) {
          this.subscribeResetInvalidInput();
        }
      }

      this.btnCloseElem = this.hostElem.querySelector('#camera-settings__head__btn-close');
      if (this.btnCloseElem) {
        this.btnCloseElem.onclick = () => this.onClose()
      }
      this.btnCancelElem = this.hostElem.querySelector('#camera-settings__nav__btn--cancel');
      if (this.btnCancelElem) {
        this.btnCancelElem.onclick = () => this.onCancel();
      }
      this.btnConfirmElem = this.hostElem.querySelector('#camera-settings__nav__btn--confirm');
      if (this.btnConfirmElem) {
        this.btnConfirmElem.onclick = () => this.onConfirm()
      }
    }

    if (formIsReady && cameraId) {
      this.apiGetModel(cameraId)
    }
  }

  defineFormIsReady() {
    let isReady = true;
    Object.keys(this.formGroup).forEach(key => {
      if (!this.formGroup[key].input || !this.formGroup[key].errorElem) {
        isReady = false;
        console.error(`Ошибка при инициализации поля формы ${ key }`);
      }
    });
    return isReady;
  }

  // прослушки событий изменения полей, для снятия невалидности
  subscribeResetInvalidInput() {
    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.length) {
        group = [group];
      }
      group.forEach(input => input.oninput = () => this.setErrorToField(key, false));
    });
  }

  // api запрос на модель камеры
  apiGetModel(cameraId) {
    this.setLoading(true);
    // api на получение модели камеры
    console.log(`Отправляем cameraId: ${ cameraId }`);
    setTimeout(() => {
      // пришла с api модель (TEST_MODEL)
      console.log(`Получаем модель:`, TEST_MODEL);
      this.camera = TEST_MODEL;
      this.fillFormByModel();
      this.setLoading(false);
    }, 1000);
  }

  // заполнение формы из модели
  fillFormByModel() {
    if (this.defineFormIsReady()) {
      Object.keys(this.formGroup).forEach(key => {
        this.formGroup[key].input.value = this.camera[key] || ''
      });
    }
  }

  // валидация формы
  formValid() {
    let isValid = true;
    Object.keys(this.formGroup).forEach(key => {
      let group = this.formGroup[key].input;
      if (!group.value) {
        this.setErrorToField(key, 'Обязательно для заполнения');
        isValid = false;
      }
    });

    return isValid;
  }

  // получение модели из формы
  getModalByForm() {
    const model = {};
    Object.keys(this.formGroup).forEach(key => {
      model[key] = this.formGroup[key].input.value;
    });
    return model;
  }

  onClose() {
    console.log('Нажата кнопка «Крестик»')
  }

  onCancel() {
    console.log('Нажата кнопка «Отмена»')
  }

  onConfirm() {
    if (this.formValid()) {
      this.camera = { ...this.camera, ...this.getModalByForm() };

      if (this.camera.id) {
        this.apiSave();
      } else {
        this.apiCreate();
      }

      console.log('Модель на отрпавку:');
      console.log(this.camera);
    }
  }

  apiSave() {
    console.log('Сохранине');

    // тут пришла ошибка от бека с невалидным полем
    const errors = TEST_ERROR.errors;
    this.parseError(errors);
  }

  apiCreate() {
    console.log('Создание');

    // тут пришла ошибка от бека с невалидным полем
    const errors = TEST_ERROR.errors;
    this.parseError(errors);
  }

  parseError(errors) {
    // бежим по ошибкам, где есть свойство field
    errors.filter(err => err.error && err.error.field).forEach(err => {
      let textError;
      // в зависимости от типа ошибки готовим её текст
      switch (err.error.type) {
        case 'INVALID_FIELD':
          textError = 'Некорректное значение';
      }

      // отдаем текст ошибки, что бы отобразить
      this.setErrorToField(err.error.field, textError);
    })
  }

  setLoading(flag) {
    if (this.defineFormIsReady()) {
      Object.keys(this.formGroup).forEach(key => {
        let group = this.formGroup[key].input;
        if (!group.length) {
          group = [group];
        }

        group.forEach(input => flag ? input.setAttribute('disabled', true) : input.removeAttribute('disabled'));
      });
    }

    if (this.btnConfirmElem) {
      flag ? this.btnConfirmElem.setAttribute('disabled', true) : this.btnConfirmElem.removeAttribute('disabled');
    }
  }

  setErrorToField(key, textError) {
    let group = this.formGroup[key].input;
    if (!group.length) {
      group = [group];
    }

    if (textError) {
      this.formGroup[key].errorElem.innerHTML = textError;
      this.formGroup[key].errorElem.classList.add('camera-settings-form__field-error--active');
      group.forEach(input => input.classList.add('invalid'));
    } else {
      this.formGroup[key].errorElem.classList.remove('camera-settings-form__field-error--active');
      group.forEach(input => input.classList.remove('invalid'));
    }
  }
}

$(function () {
  // в консруктор можно передать cameraId. Сработает метод apiGetModel якобы API на получение и заполнит форму (new CameraSettings(5);)
  new CameraSettings(1);
});
