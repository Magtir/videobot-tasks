class Stopwatch {
  constructor() {
    // init полей
    this.isActive = false;

    this.hostElem = document.querySelector('#stopwatch-wrapper-dz');
    this.optionSelectedElem;
    this.optionAdditionalElem;
    this.optionIsOpen = false;

    this.inputElem;
    this.inputIsValid = true;

    this.btnStartOrStopElem;
    this.btnIntervalElem;

    this.isRevertMod = false;

    this.timer;
    this.currentMs = 0;
    this.startMs = 0;

    this.isHide = false;

    this.isPressCtrl = false;
    this.isPressShift = false;
    // init полей end

    if (this.hostElem) {
      this.optionSelectedElem = this.hostElem.querySelector('#stopwatch__selected');
      this.optionAdditionalElem = this.hostElem.querySelector('#stopwatch__select__option');
      this.inputElem = this.hostElem.querySelector('#stopwatch__input');
      this.btnStartOrStopElem = this.hostElem.querySelector('#stopwatch__btn-start-or-stop');
      this.btnIntervalElem = this.hostElem.querySelector('#stopwatch__btn-interval');
    }

    if (this.optionSelectedElem && this.optionAdditionalElem && this.inputElem && this.btnStartOrStopElem && this.btnIntervalElem) {
      this.optionSelectedElem.onclick = () => {
        this.toggleOpenOption();
      };

      this.initInput();
      this.initBtnStartOrStop();
      this.initBtnInterval();
      this.initKeyboardListener();
    } else {
      console.error('Не все элементы страницы найдены');
    }
  }

  toggleOpenOption() {
    if (!this.optionIsOpen) {
      this.openOption();
    } else {
      this.closeOption();
    }
  }

  openOption() {
    this.optionSelectedElem.classList.add('open');
    this.optionAdditionalElem.classList.add('open');

    this.optionAdditionalElem.onclick = () => {
      this.isRevertMod = !this.isRevertMod;
      this.afterChangeMode();
    };
    this.optionIsOpen = true;
  }

  closeOption() {
    this.optionSelectedElem.classList.remove('open');
    this.optionAdditionalElem.classList.remove('open');
    this.optionIsOpen = false;
  }

  afterChangeMode() {
    this.closeOption();
    this.optionSelectedElem.innerText = this.isRevertMod ? 'Обратный отсчет' : 'Секундомер';
    this.optionAdditionalElem.innerText = this.isRevertMod ? 'Секундомер' : 'Обратный отсчет';
    if (this.isRevertMod) {
      this.inputIsValid = false;
      this.inputElem.focus();
    } else {
      this.inputElem.value = '00:00:00';
      this.inputIsValid = true;
    }
    this.afterChangeInputValid();
    this.inputReadonly(!this.isRevertMod);
  }

  inputReadonly(flag) {
    if (flag) {
      this.inputElem.setAttribute('readonly', true);
    } else {
      this.inputElem.value = '';
      this.inputElem.removeAttribute('readonly')
    }
  }

  initInput() {
    this.inputElem.oninput = inputData => {
      this.inputValid(inputData);
    }
  }

  removeEnd(count = 1) {
    this.inputElem.value = this.inputElem.value.substring(0, this.inputElem.value.length - count);
  }

  inputValid(inputData) {
    this.inputIsValid = false;
    if (this.inputElem.value) {
      if (!inputData || inputData.data !== null && +inputData.data >= 0) {
        if ((this.inputElem.value.length === 3 && this.inputElem.value[2] !== ':') || (this.inputElem.value.length === 6 && this.inputElem.value[5] !== ':')) {
          this.removeEnd();
          this.inputElem.value += `:${ inputData.data }`;
        }
        if (this.inputElem.value.length === 2) {
          if (+this.inputElem.value > 60) {
            this.removeEnd(2);
          } else {
            this.inputElem.value += ':';
          }
        }
        if (this.inputElem.value.length === 5) {
          if (+(`${ this.inputElem.value[3] }${ this.inputElem.value[4] }`) > 60) {
            this.removeEnd(2);
          } else {
            this.inputElem.value += ':';
          }
        }

        if (this.inputElem.value.length === 8) {
          if (+(`${ this.inputElem.value[6] }${ this.inputElem.value[7] }`) > 60) {
            this.removeEnd(2);
          } else {
            this.inputElem.value += ':';
          }
        }

        if (this.inputElem.value.length > 8) {
          this.removeEnd();
        }
      } else {
        if (inputData && inputData.data !== null) {
          this.removeEnd();
        }
      }
    }

    if (this.inputElem.value.length === 8 && this.inputElem.value !== '00:00:00') {
      const arr = this.inputElem.value.split(':');
      let isValid = true;
      if (arr.length === 3) {
        arr.forEach(a => {
          if (!(+a[0] >= 0 && +a[1] >= 0 && +a <= 60)) {
            isValid = false;
          }
        })
      } else {
        isValid = false;
      }

      if (!isValid) {
        this.inputElem.value = '';
      } else {
        this.inputIsValid = true;
      }
    }

    this.afterChangeInputValid();
  }

  afterChangeInputValid() {
    this.inputIsValid ? this.btnStartOrStopElem.removeAttribute('disabled') : this.btnStartOrStopElem.setAttribute('disabled', 'disabled');
  }

  initBtnStartOrStop() {
    this.btnStartOrStopElem.onclick = () => {
      this.toggleStartOrStop();
    }
  }

  initBtnInterval() {
    this.btnIntervalElem.onclick = () => {
      if (this.isActive) {
        this.getInterval();
      }
    }
  }

  initKeyboardListener() {
    window.addEventListener('keydown', e => {
      switch (e.code) {
        case 'ControlLeft':
          this.isPressCtrl = true;
          break;

        case 'ShiftLeft':
          this.isPressShift = true;
          break;

        case 'KeyV':
          if (this.isPressCtrl && this.isPressShift) {
            this.toggleVisibilityStopwatch();
          }
          break;

        case 'KeyS':
          if (!this.isHide) {
            if (this.isPressCtrl && this.isPressShift) {
              this.toggleStartOrStop();
            }
          }
          break;

        case 'KeyZ':
          if (!this.isHide) {
            if (this.isPressCtrl && this.isPressShift) {
              this.getInterval();
            }
          }
          break;
      }
    });

    window.addEventListener('keyup', e => {
      switch (e.code) {
        case 'ControlLeft':
          this.isPressCtrl = false;
          break;

        case 'ShiftLeft':
          this.isPressShift = false;
          break;
      }
    })
  }

  toggleVisibilityStopwatch() {
    this.isHide = !this.isHide;
    if (this.isHide) {
      this.hostElem.classList.add('stopwatch-wrapper--hide');
    } else {
      this.hostElem.classList.remove('stopwatch-wrapper--hide');
    }
  }

  afterChangeIsActive() {
    this.isActive ? this.btnStartOrStopElem.innerText = 'Стоп' : this.btnStartOrStopElem.innerText = 'Старт';
    this.isActive ? this.btnStartOrStopElem.classList.add('active') : this.btnStartOrStopElem.classList.remove('active');
    this.isActive ? this.btnIntervalElem.removeAttribute('disabled') : this.btnIntervalElem.setAttribute('disabled', 'disabled');
    this.isActive ? this.optionSelectedElem.classList.add('disabled') : this.optionSelectedElem.classList.remove('disabled');

    if (this.isActive) {
      this.inputElem.setAttribute('readonly', true);
    } else {
      if (this.isRevertMod) {
        this.inputElem.removeAttribute('readonly');
      }
    }
  }

  msToTime(duration, format = 'h:m:s') {

    let seconds = parseInt(String((duration / 1000) % 60), 0);
    let minutes = parseInt(String((duration / (1000 * 60)) % 60), 0);
    let hours = parseInt(String((duration / (1000 * 60 * 60))), 0);

    hours = (hours < 10) ? '0' + hours : hours;
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds : seconds;

    let formatTime = '';
    if (format.indexOf('h') > -1) {
      formatTime += hours;
    }

    if (format.indexOf('m') > -1) {
      if (formatTime !== '') {
        formatTime += ':';
      }
      formatTime += minutes;
    }

    if (format.indexOf('s') > -1) {
      if (formatTime !== '') {
        formatTime += ':';
      }
      formatTime += seconds;
    }

    return formatTime;
  }

  getInterval() {
    if (this.isActive) {
      console.log('Interval: ', Math.abs(this.startMs - this.currentMs));
      this.startMs = this.currentMs;
    }
  }

  toggleStartOrStop() {
    if (this.isRevertMod) {
      this.inputValid();
    }
    if (!this.isRevertMod || this.inputIsValid || this.isActive) {
      this.isActive = !this.isActive;
      this.afterChangeIsActive();
      this.isActive ? this.start() : this.stop();
    }
  }

  start() {
    if (!this.isRevertMod) {
      this.inputElem.value = '00:00:00';
    }

    this.closeOption();

    const arr = this.inputElem.value.split(':');
    this.currentMs += +arr[0] * 60 * 60 * 1000;
    this.currentMs += +arr[1] * 60 * 1000;
    this.currentMs += +arr[2] * 1000;

    this.startMs = this.currentMs;

    this.timer = setInterval(() => {
      if (!this.isRevertMod) {
        this.currentMs += 1000;
      } else {
        this.currentMs -= 1000;
      }

      this.inputElem.value = this.msToTime(this.currentMs);

      if (this.currentMs <= 0) {
        this.currentMs = 0;
        this.toggleStartOrStop();
      }
    }, 1000);

    console.log('start');
  }

  stop() {
    clearInterval(this.timer);
    console.log(`Остановка на ${ this.currentMs }`);
    this.currentMs = 0;

    if (this.isRevertMod) {
      this.inputValid();
    }
  }

  reset() {
    if (this.isActive) {
      this.toggleStartOrStop();
    }
    this.isRevertMod = false;
    this.afterChangeMode();
  }
}

$(function () {
  new Stopwatch();
});
