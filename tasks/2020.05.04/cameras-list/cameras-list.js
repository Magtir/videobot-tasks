const TEST_DATA = {
  data: [
    { id: 1, name: 'Камера 1', ip: '192.12.3', microphone: false, notification: true, callback: false },
    { id: 2, name: 'Камера 2', ip: '192.12.3', microphone: true, notification: true, callback: false },
    { id: 3, name: 'Камера 3', ip: '192.12.3', microphone: true, notification: true, callback: true },
    { id: 4, name: 'Камера 4', ip: '192.12.3', microphone: false, notification: false, callback: false },
    { id: 5, name: 'Камера 5', ip: '192.12.3', microphone: false, notification: true, callback: false },
    { id: 6, name: 'Камера 6', ip: '192.12.3', microphone: true, notification: true, callback: false },
    { id: 7, name: 'Камера 7', ip: '192.12.3', microphone: true, notification: true, callback: true },
    { id: 8, name: 'Камера 8', ip: '192.12.3', microphone: false, notification: false, callback: false },
  ],
  totalItems: 8,
  totalPages: 1,
};

class CamerasList {
  constructor() {
    // init полей
    this.hostElem = document.querySelector('#cameras-list-dz');
    this.tableBodyElem;
    this.tableMessageElem;
    this.btnAddElem;
    this.isLoading = false;
    this.isLastPage = false;

    this.cameras = [];
    // init полей end

    if (this.hostElem) {
      this.tableBodyElem = this.hostElem.querySelector('#cameras-list__table-body');
      this.btnAddElem = this.hostElem.querySelector('#cameras-list-btn-add');
    }

    if (this.tableBodyElem) {
      this.tableBodyElem.onscroll = () => {
        if (!this.isLastPage && !this.isLoading) {
          if (this.tableBodyElem.scrollTop + this.tableBodyElem.clientHeight >= this.tableBodyElem.scrollHeight) {
            this.apiGetCamerasList();
          }
        }
      };

      this.apiGetCamerasList();
    }

    if (this.btnAddElem) {
      this.btnAddElem.onclick = () => {
        console.log('Добавление камеры');
      }
    }
  }

  // получение списка камер (типо api запрос)
  apiGetCamerasList() {
    this.setLoading(true);
    setTimeout(() => {
      // тут пришли данные с сервера (TEST_DATA)
      this.parseData(TEST_DATA);
      this.setLoading(false);
      if (!this.cameras.length) {
        this.createMessageElem('Ничего не найдено');
      }
    }, 1000)
  }

  parseData(data) {
    if (this.tableBodyElem) {
      this.cameras.push(...data.data);
      data.data.forEach(camera => {
        const tableRow = document.createElement('div');
        tableRow.setAttribute('class', 'cameras-list__table-row cameras-list__table-row--body');
        tableRow.innerHTML = `
          <div class="cameras-list__table-cell cameras-list__table-cell--body">
            ${ camera.name }
          </div>
          <div class="cameras-list__table-cell cameras-list__table-cell--body">
            ${ camera.ip }
          </div>
          <div class="cameras-list__table-cell cameras-list__table-cell--body cameras-list__table-cell--center">
            <label class="cameras-list__table__label">
              <input type="checkbox" class="cameras-list__table__input" ${ camera.microphone ? 'checked' : '' }
                     data-id="${ camera.id }" data-name="microphone">
              <span class="cameras-list__table__input__content"></span>
            </label>
          </div>
          <div class="cameras-list__table-cell cameras-list__table-cell--body cameras-list__table-cell--center">
            <label class="cameras-list__table__label">
              <input type="checkbox" class="cameras-list__table__input" ${ camera.notification ? 'checked' : '' }
                     data-id="${ camera.id }" data-name="notification">
              <span class="cameras-list__table__input__content"></span>
            </label>
          </div>
          <div class="cameras-list__table-cell cameras-list__table-cell--body cameras-list__table-cell--center">
            <label class="cameras-list__table__label">
              <input type="checkbox" class="cameras-list__table__input" ${ camera.callback ? 'checked' : '' }
                     data-id="${ camera.id }" data-name="callback">
              <span class="cameras-list__table__input__content"></span>
            </label>
          </div>`;
        tableRow.onclick = e => {
          if (e.target.classList.contains('cameras-list__table-cell--body')) {
            console.log(`Редактирование камеры ID: ${ camera.id }`);
          }
        };
        const inputs = tableRow.querySelectorAll('.cameras-list__table__input');
        inputs.forEach(input => {
          input.onchange = () => {
            if (input.checked) {
              input.setAttribute('checked', input.checked);
            } else {
              input.removeAttribute('checked');
            }
            const id = input.getAttribute('data-id');
            const name = input.getAttribute('data-name');
            console.log(`У камеры ID: ${ id } поменято «${ name }» на ${ input.checked }`);
          }
        });
        this.tableBodyElem.appendChild(tableRow);
      });

      this.isLastPage = this.cameras.length >= data.totalItems;
    }
  }

  setLoading(flag) {
    this.isLoading = flag;
    if (flag) {
      if (this.tableMessageElem) {
        this.tableMessageElem.remove();
      }

      this.createMessageElem('Загрузка...')
    } else {
      if (this.tableMessageElem) {
        this.tableMessageElem.remove();
      }
    }
  }

  createMessageElem(text) {
    if (this.tableBodyElem) {
      this.tableMessageElem = document.createElement('div');
      this.tableMessageElem.setAttribute('id', 'cameras-list__table__message');
      this.tableMessageElem.setAttribute('class', 'cameras-list__table__message');
      this.tableMessageElem.innerText = text;
      this.tableBodyElem.appendChild(this.tableMessageElem);
    }
  }
}

$(function () {
  new CamerasList();
});
